# LyX-template-for-economics

Primer and sample for LyX (http://lyx.org), a manuscript processor with math and bibliography support.

The sample file is useful because I find a few non-default details are essential to starting off well if new to LyX.

Here's [the PDF](https://wellbeing.research.mcgill.ca/publications/lyx-intro-for-economics.pdf) which [this template](https://gitlab.com/cpbl/LyX-template-for-economics/-/tree/master) produces; it is also the instructional document to get you started.

A good place to start is with my [video introduction to this LyX template](https://www.youtube.com/watch?v=6ebg210kuqE&ab_channel=ChrisBarrington-Leigh).
